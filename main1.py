from flask import Flask,render_template,redirect
import datetime

def date():
    dateObj= datetime.datetime.now()
    return dateObj
def suffix():
    dateTime=date()
    day =int(dateTime.strftime("%d"))
    if 4 <= day <= 20 or 24 <= day <= 30:
        suffix = "th"
    else:
        suffix = ["st", "nd", "rd"][day % 10 - 1]
    return suffix
def image():
    return "https://picsum.photos/200"

def jsonobj():
    dateTime=date()
    return {
        "year": dateTime.year,
        "month": dateTime.strftime("%b"),
        "day": dateTime.strftime("%d"),
        "suffix": suffix(),
        "image": image(),
    }

def a(app): 
    @app.route("/assignment")
    def create_app():
         headTitle="Sample Page"
         json=jsonobj()
         return render_template('index.html',headTitle=headTitle,json=json)
    @app.route('/image')
    def getrandom():
         return redirect('/assignment')




