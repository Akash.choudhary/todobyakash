from email.policy import default
from pickle import FALSE, TRUE
from flask import Flask, redirect, render_template, request, session
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
#initlize the database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///todo.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
todoDatabase = SQLAlchemy(app)

class todoModel(todoDatabase.Model):
    SN = todoDatabase.Column(todoDatabase.Integer, primary_key=True)
    task = todoDatabase.Column(todoDatabase.String(100), nullable=False)
    desc = todoDatabase.Column(todoDatabase.String(500), nullable=False)
    time = todoDatabase.Column(todoDatabase.DateTime, default=datetime.utcnow)
    def __repr__(self):
        return f"{self.task}-{self.desc}"

@app.route("/", methods=['GET', 'POST'])
def todo():
    if request.method=="POST":
         task =request.form['task']
         desc=request.form['desc']
         todoData= todoModel(task = task, desc = desc)
         todoDatabase.session.add(todoData)
         todoDatabase.session.commit()
    todoShow = todoModel.query.all()
    return render_template('todo.html',todoShow=todoShow)
   

@app.route("/delete/<int:sn>")
def delete(sn):
     todoShow = todoModel.query.filter_by(SN=sn).first()
     todoDatabase.session.delete(todoShow)
     todoDatabase.session.commit()
     return redirect('/')


@app.route("/update/<int:sn>", methods=['GET', 'POST'])
def update(sn):
     todoShow = todoModel.query.filter_by(SN=sn).first()
     if request.method=="POST":
         task =request.form['task']
         desc=request.form['desc']
         todoShow.task=task
         todoShow.desc=desc
         todoDatabase.session.add(todoShow)
         todoDatabase.session.commit()
         return redirect('/')
     return render_template('update.html',todoShow=todoShow)
     
import main1
main1.a(app)
if __name__=="__main__":
    app.run(debug=True)
